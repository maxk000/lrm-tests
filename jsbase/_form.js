const assert = require('assert');

let Form = {
	loginFillAndSubmit: function( login, password ) {
		browser.pause( 1000 );

		browser.execute(() => {
			window.is_lrm_testing = true;
			// Display login modal
			jQuery(document).triggerHandler('lrm_show_login');
		}, );

		const opened_modal = $(".lrm-user-modal.is-visible");
		opened_modal.waitForExist(6000);
		assert.equal( opened_modal.isExisting() , true );

		let username_inp = $(".lrm-signin-section input[name='username']")
		username_inp.setValue( login );

		let pass_inp = $(".lrm-signin-section input[name='password']")
		pass_inp.setValue( password );

		//console.log( "username_inp.getValue", username_inp.getValue() );

		$('.lrm-signin-section .fieldset--submit button').click();

		return this._getResponse();
	},

	_getResponse: function() {
		browser.pause( 3000 );

		let lrm_response = browser.execute(() => {
			// browser context - you may not access client or console
			return window.lrm_response;
		}, );

		console.log( "lrm_response", lrm_response );
		// Example: { data: { for: 'username', message: 'Invalid username!' },   success: false }
		// Usage: lrm_response.success
		// Usage: lrm_response.data

		return lrm_response;
	}
};

module.exports = Form;