const assert = require('assert');

let config = {
	base_url: 'https://dev.maxim-kaminsky.com/'
};

config.api_url = config.base_url+"wp-content/plugins/lrm-tests/php/call_wp.php?pass=srMrtopm&";

config.option_url = config.api_url+"action=set_options&options=";
config.set_after_action_url = config.api_url+"action=set_after_action&type=";
config.post_meta_url = config.api_url+"action=set_meta&options=";
config.user_url = config.api_url+"action=get_user&options=";
config.user_roles_url = config.api_url+"action=get_user_roles&options=";

config.api_call = function( action, params, id_or_val ) {
	let url = "";
	switch (action) {
		case "options":
			url = config.option_url + params;
			break;
		case "post_meta":
			url = config.post_meta_url + "&post_id=" + id_or_val;
			break;
		case "set_action":
			// Example: params="login", id_or_val="reload"
			// Example: params="register_verification_pro", id_or_val="back"
			// Example: params="register_verification_pro", id_or_val="login"
			url = config.set_after_action_url + params + "&action=" + id_or_val;
			break;
		case "user":
			url = config.user_url + params  + "&user_id=" + id_or_val;
			break;
		case "user_roles":
			url = config.user_roles_url + params  + "&user_id=" + id_or_val;
			break;
	}

	//console.log( url );

	if ( !url ) {
		console.log( "No Url!", url );
		return;
	}

	browser.url(  url + '&pass=srMrtopm' );

	//browser.pause( 1000 );

	let api_result = browser.execute(() => {
		// browser context - you may not access client or console
		return window.api_result
	}, )
	// node.js context - client and console are available
	console.log("api_result", api_result) // outputs: 10

	if ( !api_result.success ) {
		console.log( api_result.errors );
		assert.equal(api_result.success, true, url);
	}

	return api_result;

	//
	// 	//const is_error = $('body*=Error');
	// 	//
	// 	// is_error.
	// 	//     console.log(k);
	// 	// });
	//
	// 	//is_error.waitForExist(1000);
	//
	// 	//browser.pause( 1000 );
	//
	// 	// console.log( "Is Error:", is_error.isExisting() );
	// 	//
	// 	// console.log( "Is Error:", is_error );
	//
	// 	// if ( is_error.isExisting() ) {
	// 	// 	console.log( "Is Error:", is_error.getText() );
	// 	// 	assert.equal( is_error.isExisting() , false, url );
	// 	// }
	//
	// });
}


module.exports = config;