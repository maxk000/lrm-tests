<?php

if ( !isset($_GET['pass']) || $_GET['pass'] !== 'srMrtopm' ) {
    echo "Error";
    die;
}

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// https://dev.maxim-kaminsky.com/wp-content/plugins/lrm-tests/php/call_wp.php?action=set_options&options=auto_trigger/general/on|auto_trigger/general/timeout|auto_trigger/general/after_n_pages|auto_trigger/general/stop_after_n_displays&values=true|1|1|0
// options=auto_trigger/general/on|0*auto_trigger/general/timeout|1*auto_trigger/general/after_n_pages|1*auto_trigger/general/stop_after_n_displays|0

//make sure we skip most of the loading which we might not need
//http://core.trac.wordpress.org/browser/branches/3.4/wp-settings.php#L99
define('SHORTINIT', true);

//mimic the actuall admin-ajax
define('DOING_AJAX', true);

// This include gives us all the WordPress functionality
$parse_uri = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
require_once( $parse_uri[0] . 'wp-load.php' );
require_once( $parse_uri[0] . 'wp-includes/post.php' );
require_once( $parse_uri[0] . 'wp-includes/revision.php' );
require_once( $parse_uri[0] . 'wp-includes/class-wp-post.php' );

?>
<!DOCTYPE html><html lang="en"><head><title>WP CALL</title></head><body>
<?php

$api_result = [
    'success' => true,
    'errors' => [],
];

$action = isset($_GET['action']) ? $_GET['action'] : '';
$options = [];


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

switch ( $action ) {
    case 'set_options':
        $options = _get_options_arr();
        _update_options( $options );
    case 'set_after_action':
	    $type = !empty($_GET['type']) ? $_GET['type'] : '';
	    $action = !empty($_GET['action']) ? $_GET['action'] : '';
	    if ( $type && $action ) {
		    _add_js_error( 'set_after_action: No user Action or Type!' );
		    echo "set_after_action: No user Action or Type!";
		    break;
	    }
	    if ( in_array($type, ['login', 'registration', 'logout']) ) {
		    $type = $type . '/action';
        } elseif ( 'email-verification-pro-after-action' === $type ) {
		    $type = 'registration/email-verification-pro-after-action';
        }

	    _update_options( ['redirects/' . $type => $action ] );

	case 'set_meta':
    	if ( !isset($_GET['post_id']) ) {
    		echo "Error!";
    		_add_js_error( 'No post ID!' );
    		break;
	    }
        $options = _get_options_arr();
        _set_meta( $options, $_GET['post_id'] );
        break;
    case 'get_user_roles':
    case 'get_user':
    	if ( !isset($_GET['user_id']) ) {
            _add_js_error( 'No user ID!' );
    		echo "Error!";
            break;
	    }
        require_once ABSPATH . 'wp-admin/includes/user.php';
        require_once ABSPATH . 'wp-includes/user.php';
        require_once ABSPATH . 'wp-includes/capabilities.php';
        require_once ABSPATH . 'wp-includes/class-wp-role.php';
        require_once ABSPATH . 'wp-includes/class-wp-roles.php';
        require_once ABSPATH . 'wp-includes/class-wp-user.php';

        $userdata = WP_User::get_data_by( 'id', $_GET['user_id'] );

        if ( !$userdata )
            return false;

        $user = new WP_User;
        $user->init( $userdata );

        if ( 'get_user_roles' === $action ) {
	        _add_js_data( 'user_roles', implode( ',', $user->roles ) );
	        echo implode( ',', $user->roles );
        } elseif ( 'get_user' === $action ) {
	        _add_js_data( 'user', $user->roles );
        }
        break;
}

function _update_options( $opts_to_update ) {

    if ( !$opts_to_update ) {
        return;
    }

    $db_options = [];
    foreach ($opts_to_update as $setting_slug => $new_value) {
        $setting_path = explode('/', $setting_slug);

        if ( count($setting_path) !== 3 ) {
            _add_js_error( 'Invalid $setting_slug: ' . $setting_slug );
            return ;
            //throw new Exception('Invalid $setting_slug: ' . $setting_slug);
        }

        $db_options[$setting_path[0]][$setting_path[1]][$setting_path[2]] = $new_value;
    }

    $option_from_bd = false;
    foreach ($db_options as $option_key => $option_data) {
        $option_from_bd = get_option('lrm_' . $option_key, []);

//            echo '<pre>';
//            echo '##From BD' . PHP_EOL;
//            var_dump($option_from_bd);
//
//            echo '##new data'. PHP_EOL;
//            var_dump($option_data);
//
        foreach ( $option_data as $section => $section_data ) {
            $option_from_bd[$section] = array_merge($option_from_bd[$section], $section_data);
        }

        update_option( 'lrm_' . $option_key, $option_from_bd );

            echo "##updated {$option_key} <br>";
            var_dump( get_option('lrm_' . $option_key, []) );
    }

}

function _set_meta( $opts_to_update, $post_ID ) {


    if ( !$opts_to_update ) {
        return;
    }


    foreach ($opts_to_update as $option_key => $option_data) {

        update_post_meta( $post_ID, $option_key, $option_data );

        echo "##updated meta {$option_key} <br>";
        var_dump( get_post_meta( $post_ID, $option_key, true ) );
    }


}

function _get_options_arr() {

    $options = !empty($_GET['options']) ? $_GET['options'] : '';
    if ( !$options ) {
        return [];
    }

    $options = explode('*', $options);
    if ( !$options ) {
        return [];
    }

    $opts_to_update= [];
    $option_arr = [];
    $values = [];

    foreach ($options as $option_str) {
        $option_arr = explode('|', $option_str);
        if ( 2 !== count($option_arr) ) {
            _add_js_error( "Invalid option: " . $option_str );
            echo "Invalid option: ", $option_str, '#', PHP_EOL;
            continue;
        }
        $opts_to_update[ $option_arr[0] ] = $option_arr[1];
    }

    return $opts_to_update;
}

function _add_js_error( $message ) {
    $api_result['success'] = false;
    $api_result['errors'][] = $message;
}
function _add_js_data( $key, $data ) {
    $api_result[$key] = $data;
}
?>
<script>
    window.api_result = <?= json_encode($api_result); ?>;
</script>
</body></html>
<?php
// var_dump( get_option( 'lrm_redirects' ) );

/* Actions setting:
 array(3) {
  ["login"]=>
  array(2) {
    ["action"]=>
    string(8) "redirect"
    ["redirect"]=>
    array(5) {
      ["role_match"]=>
      array(3) {
        [0]=>
        string(3) "all"
        [1]=>
        string(6) "any_of"
        [2]=>
        string(6) "any_of"
      }
      ["roles"]=>
      array(3) {
        [0]=>
        array(9) {
          [0]=>
          string(13) "administrator"
          [1]=>
          string(6) "editor"
          [2]=>
          string(6) "author"
          [3]=>
          string(11) "contributor"
          [4]=>
          string(10) "subscriber"
          [5]=>
          string(8) "customer"
          [6]=>
          string(12) "shop_manager"
          [7]=>
          string(7) "pending"
          [8]=>
          string(11) "phmm_client"
        }
        [1]=>
        array(1) {
          [0]=>
          string(13) "administrator"
        }
        [2]=>
        array(1) {
          [0]=>
          string(8) "customer"
        }
      }
      ["redirect"]=>
      array(4) {
        [0]=>
        string(4) "page"
        [1]=>
        string(4) "page"
        [2]=>
        string(3) "url"
        ["default"]=>
        string(3) "url"
      }
      ["redirect_url"]=>
      array(4) {
        [0]=>
        string(6) "/cont/"
        [1]=>
        string(0) ""
        [2]=>
        string(14) "/recust-login/"
        ["default"]=>
        string(19) "/no-login-redirect/"
      }
      ["redirect_page"]=>
      array(4) {
        [0]=>
        string(3) "327"
        [1]=>
        string(3) "329"
        [2]=>
        string(0) ""
        ["default"]=>
        string(0) ""
      }
    }
  }
  ["registration"]=>
  array(3) {
    ["action"]=>
    string(8) "redirect"
    ["email-verification-pro-after-action"]=>
    string(8) "redirect"
    ["redirect"]=>
    array(3) {
      ["redirect"]=>
      array(1) {
        ["default"]=>
        string(4) "page"
      }
      ["redirect_url"]=>
      array(1) {
        ["default"]=>
        string(50) "https://dev.maxim-kaminsky.com/after-registration/"
      }
      ["redirect_page"]=>
      array(1) {
        ["default"]=>
        string(3) "333"
      }
    }
  }
  ["logout"]=>
  array(2) {
    ["action"]=>
    string(8) "redirect"
    ["redirect"]=>
    array(5) {
      ["role_match"]=>
      array(1) {
        [1]=>
        string(3) "all"
      }
      ["roles"]=>
      array(1) {
        [1]=>
        array(9) {
          [0]=>
          string(13) "administrator"
          [1]=>
          string(6) "editor"
          [2]=>
          string(6) "author"
          [3]=>
          string(11) "contributor"
          [4]=>
          string(10) "subscriber"
          [5]=>
          string(8) "customer"
          [6]=>
          string(12) "shop_manager"
          [7]=>
          string(7) "pending"
          [8]=>
          string(11) "phmm_client"
        }
      }
      ["redirect"]=>
      array(2) {
        [1]=>
        string(3) "url"
        ["default"]=>
        string(4) "page"
      }
      ["redirect_url"]=>
      array(2) {
        [1]=>
        string(44) "https://dev.maxim-kaminsky.com/after-logout/"
        ["default"]=>
        string(10) "/logouted/"
      }
      ["redirect_page"]=>
      array(2) {
        [1]=>
        string(0) ""
        ["default"]=>
        string(3) "335"
      }
    }
  }
}
 */