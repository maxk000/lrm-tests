const assert = require('assert');
const config = require('./../jsbase/_base.js');

describe('LRM Auto-trigger', () => {

	// Test global

	it('Auto-trigger is triggering', () => {

		config.api_call( "options", "auto_trigger/general/on|1*auto_trigger/general/timeout|1*auto_trigger/general/after_n_pages|1*auto_trigger/general/stop_after_n_displays|0" );

		browser.pause( 1000 );

		browser.url(config.base_url);

		browser.pause( 1000 );

		const opened_modal = $(".lrm-user-modal.is-visible");
		opened_modal.waitForExist(6000);
		assert.equal( opened_modal.isExisting() , true );
	});

	it('Auto-trigger in post is triggering', () => {

		let test_result = browser.execute(() => {
			// browser context - you may not access client or console
			window.testss = 1;
			return window.testss;
		}, )

		console.log( "test_result", test_result );

	});

	// it('Auto-trigger in post is triggering', () => {
	// 	// Test postmeta
	// 	config.api_call( "post_meta", "lrm_auto_trigger_on|1*lrm_auto_trigger_timeout|1", "504" );
	//
	// 	browser.url( config.base_url + "auto-trigger/" );
	//
	// 	browser.pause( 1000 );
	//
	// 	const opened_modal = $(".lrm-user-modal.is-visible");
	// 	opened_modal.waitForExist(6000);
	// 	assert.equal( opened_modal.isExisting() , true );
	// });



});