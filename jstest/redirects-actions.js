const assert = require('assert');
const config = require('./../jsbase/_base.js');
const Form = require('./../jsbase/_form.js');

describe('LRM Actions', () => {

	it('Test fail login', () => {

		browser.url(config.base_url);
		let testLogin = Form.loginFillAndSubmit( "test", "pass" );

		assert.equal( testLogin.success, false );
		if ( !testLogin.success ) {
			// Check for the error message
			assert.equal( $(".lrm-signin-section .lrm-error-message.is-visible").isExisting(), true );
		}

	});

	it('Test success login & redirect for Customer', () => {

		browser.url(config.base_url);
		// Put here Customer login and pass
		let testLogin = Form.loginFillAndSubmit( "Customer", "CustomerTestPass911" );

		if ( testLogin.success ) {
			// Check for the error message
			assert.equal( $(".lrm-signin-section .lrm-error-message.is-visible").isExisting(), true );

			// Check redirect URL
			if ( testLogin.redirect_url= "redirect" && testLogin.redirect_url ) {
				assert.equal( testLogin.action, "/customer-redirect/" );
			}
		}

	});

});

/*
{
	data: {
		action: "redirect"
		exec_time: "Executed for 0.01608 seconds"
		logged_in: true
		message: "Login successful, you can close this window."
		redirect_url: "https://dev.maxim-kaminsky.com/reaushop-login/"
		user_id: 1
	}
	success: true
}
*/